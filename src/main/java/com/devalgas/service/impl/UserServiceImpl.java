package com.devalgas.service.impl;

import com.devalgas.entities.User;
import com.devalgas.exception.UserNotFoundException;
import com.devalgas.repository.UserRepository;
import com.devalgas.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author devalgas kamga on 23/02/2023. 18:03
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findOne(long id) {
        log.debug("find user by id : {}", id);
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        } else {
            throw new UserNotFoundException("User not found with id " + id);
        }
    }
}
