package com.devalgas.service.impl;

import com.devalgas.entities.Product;
import com.devalgas.repository.ProductRepository;
import com.devalgas.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author devalgas kamga on 23/02/2023. 18:03
 */
@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> findByPriceGreaterThan(double price) {
        return productRepository.findByPriceGreaterThan(price);
    }

    @Override
    public List<Product> getProductsByPrice(double price) {
        List<Product> filteredProducts = new ArrayList<>();
        for (Product product : productRepository.findAll()) {
            if (product.getPrice() > price) {
                filteredProducts.add(product);
            }
        }
        return filteredProducts;
    }

    @Override
    public double calculateTotal(List<Product> products) {
//        double total = 0;
//        for (Product product : products) {
//            total += product.getPrice();
//        }
//        return Math.round(total * 100.0) / 100.0;

        double total = 0;
        for (Product product : products) {
            total += product.getPrice();
        }
        return total;
    }
}
