package com.devalgas.service;

import com.devalgas.entities.Product;

import java.util.List;

/**
 * @author devalgas kamga on 23/02/2023. 18:04
 */
public interface ProductService {
    List<Product> findByPriceGreaterThan(double price);

    List<Product> getProductsByPrice(double price);
    double calculateTotal(List<Product> products);
}
