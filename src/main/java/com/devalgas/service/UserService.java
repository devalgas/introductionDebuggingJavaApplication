package com.devalgas.service;

import com.devalgas.entities.User;

/**
 * @author devalgas kamga on 23/02/2023. 18:04
 */
public interface UserService {
    User findOne(long id);

}
