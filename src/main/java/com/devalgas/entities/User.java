package com.devalgas.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.Instant;

/**
 * @author devalgas kamga on 23/02/2023. 17:55
 */

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    private Long id;
    @Column(nullable = false)
    private String pseudo;
    private boolean active;
    private Instant expiryDate;

}
