package com.devalgas.ressource;

import com.devalgas.entities.User;
import com.devalgas.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author devalgas kamga on 23/02/2023. 22:04
 */

@RestController
@Slf4j
@RequestMapping("/users")
public class UserRessource {

    private final UserService userService;

    public UserRessource(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{userId}/")
    public ResponseEntity<User> getUserById(@PathVariable Long userId){
        if (userId == null || userId <= 0) {
            throw new IllegalArgumentException("User ID is invalid : " + userId);
        }
        return ResponseEntity.ok(userService.findOne(userId));
    }
}
