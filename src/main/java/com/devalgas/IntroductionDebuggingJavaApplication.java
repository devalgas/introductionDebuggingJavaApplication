package com.devalgas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class  IntroductionDebuggingJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntroductionDebuggingJavaApplication.class, args);
	}

}
