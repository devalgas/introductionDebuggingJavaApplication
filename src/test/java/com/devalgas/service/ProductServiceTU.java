package com.devalgas.service;

import com.devalgas.entities.Product;
import com.devalgas.repository.ProductRepository;
import com.devalgas.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author devalgas kamga on 23/02/2023. 18:13
 */
@ExtendWith(MockitoExtension.class)
public class ProductServiceTU {
    @InjectMocks
    private ProductServiceImpl productService;

    @Mock
    private ProductRepository productRepository;

    @Captor
    private ArgumentCaptor<Product> productArgumentCaptor;

    @BeforeEach
    void setUp() {
    }

    @Nested
    class ProductSearch{

        @Test
        @DisplayName("Should get the list of the best products at a given price")
        void shouldSuperiorProductsPriceArPrice() {
            //Arrange
            List<Product> products = List.of(Product.builder().price(50).build(), Product.builder().price(200).build(),
                    Product.builder().price(300).build());
            when(productRepository.findByPriceGreaterThan(100)).thenReturn(products.subList(1, 3));

            //Act
            List<Product> result = productService.findByPriceGreaterThan(100);

            //Assert
            verify(productRepository).findByPriceGreaterThan(100);
            assertThat(result).isNotNull();
            assertThat(result.size()).isEqualTo(2);
            assertThat(result.get(0).getPrice()).isEqualTo(200);
        }
    }

}
