package com.devalgas.service;

import com.devalgas.entities.Product;
import com.devalgas.entities.User;
import com.devalgas.exception.UserNotFoundException;
import com.devalgas.repository.ProductRepository;
import com.devalgas.repository.UserRepository;
import com.devalgas.service.impl.ProductServiceImpl;
import com.devalgas.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author devalgas kamga on 23/02/2023. 18:13
 */
@ExtendWith(MockitoExtension.class)
public class UserServiceTU {
    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Captor
    private ArgumentCaptor<User> userArgumentCaptor;

    @BeforeEach
    void setUp() {
    }

    @Nested
    class userSearch{

        @Test
        @DisplayName("Should make find user by id and return error 404")
        void shouldUserByIdNotFoundException() {
            //Arrange
            long userId = 1L;
            when(userRepository.findById(userId)).thenReturn(Optional.empty());

            assertThrows(UserNotFoundException.class, () -> {
                userService.findOne(userId);
            });
        }
    }

}
